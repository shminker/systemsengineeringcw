/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 * FXML Controller class
 *
 * @author admin
 */
public class controller_login implements Initializable {
    private final Image mainIcon = new Image(getClass().getResourceAsStream(
                       "Resources/img/icons/thought.png"));
    
    @FXML
    private TextField tfUsername;
    @FXML
    private PasswordField pfPassword; 
    /**
     * Initializes the controller class.
     */
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    } 
    
    public void login(){
        if (!tfUsername.getText().isEmpty() &&
                !pfPassword.getText().isEmpty()){
            if(tfUsername.getText().equals("admin") &&
                    pfPassword.getText().equals("admin")){
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_main.fxml"));
                    Parent root1 = (Parent) fxmlLoader.load();                      
                    Stage stage = new Stage();
                    stage.getIcons().add(mainIcon);
                    stage.setTitle("Concrete Study");
                    stage.setScene(new Scene(root1));  
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.show();
                    stage.setOnCloseRequest(e -> Platform.exit());
                    close();
                }
                catch (Exception e){   

                }
            }else{
                tfUsername.clear();
                pfPassword.clear();
            }
        }
    }
    public void close(){
        Stage stage = (Stage) tfUsername.getScene().getWindow();
        stage.close();
    }   
        
}
