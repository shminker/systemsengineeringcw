/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy.Models;

import java.io.Serializable;
import java.sql.Time;
import java.time.Duration;
import java.util.Date;

/**
 *
 * @author admin
 */
public class Activity implements Serializable {
    
    private String name;
    private int timeSpent; 
    private String notes;    
    private int completion; //ak
    private Duration duration;
    private Duration remaining;
    private String timeString;
    private Boolean complete;
    private enum Type {Read, Write, Programme}
    private String type;
    
    
    
    
    public Activity(String name, String type, Duration duration, String note){
        this.name = name;
        this.type = type;
        this.duration = duration;
        this.remaining  = duration;
        this.notes = note;
        complete = false;
        timeString = String.format("%02d:%02d:%02d", 
                remaining.toHours(),
                remaining.toMinutes()%60,
                remaining.getSeconds()%60);

    }
    
    public Boolean isComplete(){
        return complete;
    }
    
    public void setComplete(Boolean inpComplete){
        complete = inpComplete;
    }
    
    
    public String getName(){
        return name;
    }

    public String getType(){
        return type;
    }
    
    public Duration getTime(){
        return duration;
    }
    
    public String getNote(){
        return this.notes;
    }
    
    public void setTimeRemaining(Duration duration){
        this.remaining = duration;
        System.out.println(getFriendlyRemainingTime());
    }
    
    public Duration getTimeRemaining(){
        return this.remaining;
        
    } 
    public String getTimeString(){
        return timeString;
    }
    
    public String getFriendlyRemainingTime(){
        return String.format("%02d:%02d:%02d", 
                remaining.toHours(),
                remaining.toMinutes()%60,
                remaining.getSeconds()%60);
    } 
    
    @Override
    public String toString(){
        return name;
    }    
    
    
}

