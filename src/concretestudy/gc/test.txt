hi

ready to + task type


profile,Natalia Grealish,CMP,Computing Science,2,244067588,2
module,Programming 2,CMP-5015Y,YL,2019-01-15 12:00,2019-06-02 03:00
assignment,Programming 2,Coursework 1,2019-01-15 12:00,2019-02-06 03:00,30,
assignment,Programming 2,Coursework 2,2019-02-28 12:00,2019-05-01 03:00,30,
assignment,Programming 2,Exam,2019-05-31 14:15,2019-05-31 16:15,40,

task,Programming 2,Coursework 1,Task 1,2019-01-19 03:00,2019-01-20 03:00,example note,t,
task,Programming 2,Coursework 1,Task 2,2019-01-25 03:00,2019-01-26 03:00,example note,t,
task,Programming 2,Coursework 1,Task 3,2019-02-03 12:00,2019-02-03 03:00,example note,t,

activity,Programming 2,Coursework 1,Task 1, Activity 1,Reading,24,sample note,t,
activity,Programming 2,Coursework 1,Task 2, Activity 1,Reading,24,sample note,t,
activity,Programming 2,Coursework 1,Task 3, Activity 1,Reading,30,sample note,t,



task,Programming 2,Coursework 2,Task 1,2019-02-01 03:00,2019-03-02 03:00,example note,f,
task,Programming 2,Coursework 2,Task 2,2019-03-03 12:00,2019-03-05 03:00,example note,f,
task,Programming 2,Coursework 2,Task 3,2019-03-10 12:00,2019-03-14 03:00,example note,t,
activity,Programming 2,Coursework 2,Task 1, Activity 1,Reading,24,sample note,t,



module,Data Structures & Algorithums,CMP-5014Y,YL,2019-01-15 12:00,2019-06-02 03:00
assignment,Data Structures & Algorithums,Coursework 1,2019-01-15 12:00,2019-02-16 03:00,30,
assignment,Data Structures & Algorithums,Coursework 2,2019-02-28 12:00,2019-03-20 03:00,30,
assignment,Data Structures & Algorithums,Exam,2019-05-24 13:15,2019-05-24 15:15,40,

task,Data Structures & Algorithums,Coursework 1,Task 1,2019-01-17 12:00,2019-01-20 03:00,example note,t,
task,Data Structures & Algorithums,Coursework 1,Task 2,2019-01-25 12:00,2019-01-28 03:00,example note,t,
task,Data Structures & Algorithums,Coursework 1,Task 3,2019-02-05 12:00,2019-02-05 03:00,example note,f,

task,Data Structures & Algorithums,Coursework 2,Task 1,2019-02-28 12:00,2019-03-01 03:00,example note,t,
task,Data Structures & Algorithums,Coursework 2,Task 2,2019-03-03 12:00,2019-03-05 03:00,example note,f,
task,Data Structures & Algorithums,Coursework 2,Task 3,2019-03-10 12:00,2019-03-14 03:00,example note,f,

module,Graphics 1,CMP-5015Y,S2,2019-01-15 12:00,2019-06-02 03:00
assignment,Graphics 1,Coursework 1,2019-02-15 12:00,2019-05-01 03:00,60,
assignment,Graphics 1,Exam,2019-05-28 13:15,2019-05-28 15:15,40,



module,Systems Engineering 1,CMP-5012B,S2,2019-01-15 12:00,2019-06-02 03:00
assignment,Systems Engineering 1,Coursework 1,2019-01-15 12:00,2019-03-15 03:00,40,
assignment,Systems Engineering 1,Coursework 2,2019-03-10 12:00,2019-05-10 03:00,60,
