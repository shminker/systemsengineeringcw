/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy.Models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import static java.util.concurrent.TimeUnit.DAYS;

/**
 *
 * @author admin
 */

public class Module implements Serializable { 
    
    
    private String name;
    private String code;
    private LocalDateTime startDate;
    private LocalDateTime endDate ;
    private ArrayList<Assignment> assignments = new ArrayList();

    public Module(String name){
        this.name = name;
    }    
    
    public Module(
            String name,
            String code,
            LocalDateTime startDate,
            LocalDateTime endDate){
        this.name = name;
        this.code = code;
        this.startDate = startDate;
        this.endDate = endDate;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }  

    
    @Override
    public String toString(){
        return name;
    }
    
    public String getCode() {
        return code;
    }

    public String getStartDate() {
        return String.format("%02d-%02d-%d",
                startDate.getDayOfMonth(),
                startDate.getMonthValue(),
                startDate.getYear());
    }
    
    public String getEndDate() {
        return String.format("%02d-%02d-%d",
                endDate.getDayOfMonth(),
                endDate.getMonthValue(),
                endDate.getYear());
    }
    
    public long length(){
        return ChronoUnit.DAYS.between(startDate, endDate);
    }
    
    public ArrayList<Assignment> getAssginments() {
        return assignments;
    }


    
    public void addAssignment(Assignment deadline) {
        this.assignments.add(deadline);
    }
    
}    

    
   