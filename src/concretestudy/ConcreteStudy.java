/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.application.Platform;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 *
 * @author ashmit.khadka
 */
public class ConcreteStudy extends Application {
    private final Image mainIcon = new Image(getClass().getResourceAsStream(
                   "Resources/img/icons/thought.png"));

    @Override
    public void start(Stage stage) throws Exception {
        Parent root = FXMLLoader.load(getClass().getResource("view_login.fxml"));        
        Scene scene = new Scene(root);        
        stage.setScene(scene);
        stage.getIcons().add(mainIcon);
        stage.setTitle("Concrete Study");
        stage.show();
        stage.setOnCloseRequest(e -> Platform.exit());
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}