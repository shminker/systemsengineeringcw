package seng;

import java.util.Calendar;

public class Assignment {
    private String      name;
    private Calendar    setDate,
                        returnDate;
    private int         weight;

    //Getters and Setters begin here
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Calendar getSetDate() {
        return setDate;
    }

    public void setSetDate(Calendar setDate) {
        this.setDate = setDate;
    }

    public Calendar getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(Calendar returnDate) {
        this.returnDate = returnDate;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    //Getters and Setters end here
    
    
    public String toString(){
        return String.format("%s====\nSet:\t%s\nDue:\t%s\nWeight:\t%s\n",
                name,
                setDate.getTime().toString(),
                returnDate.getTime().toString(),
                weight
        );
    }
    
    
    public void addExtension(Calendar newDeadline){
        returnDate = newDeadline;
    }
    
    public void addExtension(int days){
        returnDate.add(Calendar.DATE, days);
    }

    public Assignment(String name,
            Calendar setDate,
            Calendar returnDate,
            int weight
    ) {
        this.name = name;
        this.setDate = setDate;
        this.returnDate = returnDate;
        this.weight = weight;
    }
    
    public Assignment(String name, Calendar returnDate, int weight){
        this.name = name;
        this.setDate = Calendar.getInstance();
        this.returnDate = returnDate;
        this.weight = weight;
    }
    
    public Assignment(){
        this.name = "Default Assignment";
        this.setDate = Calendar.getInstance();
        this.returnDate = Calendar.getInstance();
        this.weight = 0;
    }
}