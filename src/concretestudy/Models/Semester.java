/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy.Models;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Semester implements Serializable {
    String name;
    String faulty;
    String course;
    int year;
    int SID;
    int semester;
    ArrayList<Module> modules = new ArrayList<>();
    
    public Semester(){}
    
    public Semester(
            String name,
            String faulty,
            String course,
            int year,
            int SID,
            int semester
            ){
        this.name = name;
        this.faulty = faulty;
        this.course = course;
        this.year = year;
        this.SID = SID;
        this.semester = semester;
        
    }
    public String getName(){
        return name;
    }
    public String getFaculty(){
        return faulty;
    }
    public String getCourse(){
        return course;
    }
    
    public int getYear(){
        return year;
    }
    
    public int getSID(){
        return SID;
    }  
    
    public int getSemester(){
        return semester;
    }    
    public void addModules(Module m){
        modules.add(m);        
    }
    
    public ArrayList<Module> getModules(){
        return modules;
    }
    
    public Assignment getAssginement(String URI){
        for(Module m : modules){
            for (Assignment a : m.getAssginments()){
                if (a.getURI().equals(URI)){
                    return a;
                }            
            }
        }
        return null;
    }
    public final void writeObject() throws IOException{
        ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("semester.bin"));
        oos.writeObject(this);
    }

    public static Semester loadSemester(FileReader fr){
        
        Semester newSemester = null;
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        String line = "";
        String cvsSplitBy = ",";
        try (BufferedReader br = new BufferedReader(fr)) {
            while ((line = br.readLine()) != null) {
                String[] data = line.split(cvsSplitBy);  
                
                if (data[0].equals("profile")){
                    newSemester = new Semester(
                            data[1],
                            data[2],
                            data[3],
                            Integer.parseInt(data[4]),
                            Integer.parseInt(data[5]),
                            Integer.parseInt(data[6])
                    );
                }
                else if (data[0].equals("module")){
                    Module m = new Module(
                            data[1],
                            data[2],
                            LocalDateTime.parse(data[4], dtf),
                            LocalDateTime.parse(data[5], dtf)
                    );
                    newSemester.addModules(m);     
                }
                else if (data[0].equals("assignment")){
                    Assignment a = new Assignment(
                            data[2],
                            LocalDateTime.parse(data[3], dtf),
                            LocalDateTime.parse(data[4], dtf),
                            Integer.parseInt(data[5])
                    );
                    for (Module m : newSemester.getModules()){
                        if (m.getName().equals(data[1])){
                            m.addAssignment(a);
                        }
                    }

                }else if (data[0].equals("task")){
                    LocalDateTime start = LocalDateTime.parse(data[4], dtf);
                    LocalDateTime end = LocalDateTime.parse(data[5], dtf);
                    Duration dur = Duration.between(start, end);
                    Task t = new Task(
                            data[3],
                            start,
                            end,
                            data[6],                            
                            dur
                    );                    
                    for (Module m : newSemester.getModules()){
                        if (m.getName().equals(data[1])){
                            for (Assignment a : m.getAssginments()){
                                if (a.getName().equals(data[2])){
                                    a.addTasks(t);
                                }
                            }
                        }
                    }

                }
                
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        for (Module m : newSemester.getModules()){
            System.out.println(m.getName());
            for (Assignment a : m.getAssginments()){
                System.out.printf("\t%s, %s, %s, %s\n",
                        a.getName(), a.getSetDate(), a.getReturnDate(), a.getWeight());

                for (Task t : a.getTasks()){
                    System.out.printf("\t\t%s\n", t.getName());
                }
                                    
                
            }
        }
        return newSemester;
    }

}
