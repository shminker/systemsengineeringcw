
package secw1;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;


public class Semester {
    
    private String name;
    private Calendar begin;
    private Calendar end;
    private int numModules; 
    private ArrayList<Module> modules;

    
    public Semester(){
        
    }
    
    public Semester(String name, String begin, String end,int numModules, ArrayList<Module> modules){
        
        
        this.name = name;
        this.begin.setTimeInMillis(Long.parseLong(begin));
        this.end.setTimeInMillis(Long.parseLong(end));
        this.numModules = numModules;
        this.modules = modules;
    }
    
    public Semester(String name, String begin, String end,int numModules){
        this.name = name;
        this.begin.setTimeInMillis(Long.parseLong(begin));
        this.end.setTimeInMillis(Long.parseLong(end));
        this.numModules = numModules;

    }
    
    public void setName(String name){
        this.name = name;
    }
    
    public void setBeing(String begin){
        this.begin.setTimeInMillis(Long.parseLong(begin));
    }
    
    public void setEnd(String end){
        this.end.setTimeInMillis(Long.parseLong(end));
    }
    
    public void setNumModules(int num){
        this.numModules = num;
    }
    
    public void setModules(ArrayList<Module> modules){
        this.modules = modules;
    }
    
    public String getName(){
        return name;
    }
    
    public Calendar getBeing(){
        return begin;
    }
    
    public Calendar getEnd(){
        return end;
    }
    
    public int getNumModules(){
        return numModules;
    }
    
    public ArrayList<Module> getModules(){
        return modules;
    }
    
    public void addModule(Module m){
        modules.add(m);
        
    }
    
    public void rmvModule(Module m){
        modules.remove(m);
    }
}
