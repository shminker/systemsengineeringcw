/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import concretestudy.Models.Module;
import concretestudy.Models.Semester;
import concretestudy.Models.Milestone;
import concretestudy.Models.Task;
import concretestudy.Models.Assignment;
import java.net.URL;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.TreeMap;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.CheckBoxTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.stage.Stage;

/**
 * FXML Controller class
 *
 * @author admin
 */
public class controller_milestone implements Initializable {

    controller_main parent_main;
    Semester parent_semster;
    Assignment parent_assignment;
    Milestone parent_milestone;
    @FXML
    private TextField tfTitle;
    @FXML
    private ComboBox cbAssignment;
    @FXML
    private Label lbStart;
    @FXML
    private Label lbEnd;
    @FXML
    private TableView tvTasks;
    @FXML
    private TableColumn tcName;
    @FXML
    private TableColumn tcSet;
    @FXML
    private TableColumn tcDue;
    @FXML
    private TableColumn tcSelect;
    @FXML
    private ImageView imgAdd;
    
    TreeMap<String, Assignment> assignments = new TreeMap<>();
    
    /**
     * Initializes the controller class.
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }   


    public void setData(Semester s){
        LocalDateTime today = LocalDateTime.now();
        parent_semster = s;
        for (Module m: parent_semster.getModules()){            
            for (Assignment a: m.getAssginments()){  
                if(!a.isComplete() && today.isBefore(a.getReturnDate())){
                    assignments.put(a.getURI(), a);
                    cbAssignment.getItems().add(a.getURI());
                }                    
            }            
        }
    }
    public void selectAssignment(){        
        parent_assignment = assignments.get(cbAssignment.getValue());
        tvTasks.setEditable(true);
        tcSelect.setEditable(true);
        ObservableList<Task> tvTasksData = 
                FXCollections.observableArrayList(); 
        for(Task t : parent_assignment.getTasks()){
            tvTasksData.add(t);
        }        
        tcName.setCellValueFactory(
            new PropertyValueFactory<Task, String>("name")); 
        tcSet.setCellValueFactory(
            new PropertyValueFactory<Task, String>("setDate")); 
        tcDue.setCellValueFactory(
            new PropertyValueFactory<Task, String>("returnDate"));      
        tcSelect.setCellValueFactory(
            new PropertyValueFactory<Task, String>("select"));         
        
        tvTasks.setItems(tvTasksData);

    }
    
    public void selectTask(){
        Object selectedItem = tvTasks.getSelectionModel().getSelectedItem();
        if (selectedItem instanceof Task){
            Task t = (Task)selectedItem;
            System.out.println(t.getName());
        }
        

    }
    
    public void addAssignment(){
        if (!tfTitle.getText().isEmpty() &&
                cbAssignment.getValue() != null){       
            ArrayList<Task> selectedTasks = new ArrayList<>();
            for(Object item : tvTasks.getItems()){
                if (item instanceof Task){
                    Task t = (Task)item;
                    if (t.getSelect().isSelected()){
                       // System.out.println(t.getName());
                        selectedTasks.add(t);
                    }
                }
            }
            Milestone milestone = new Milestone(
                    tfTitle.getText(),
                    cbAssignment.getValue().toString(),
                    selectedTasks
            );
            parent_assignment.addMilestone(milestone);
            close();
        } else {
            System.out.println("try again");
        }        
    }
    
    public void showMilestone(Milestone m){
        parent_milestone = m;
        tfTitle.setText(parent_milestone.getName());
        cbAssignment.setValue(parent_milestone.getAssignmnet());
        
        ObservableList<Task> tvTasksData = 
                FXCollections.observableArrayList(); 
        for(Task t : parent_milestone.getTasks()){
            tvTasksData.add(t);
        }   
        tcName.setCellValueFactory(
            new PropertyValueFactory<Task, String>("name")); 
        tcSet.setCellValueFactory(
            new PropertyValueFactory<Task, String>("setDate")); 
        tcDue.setCellValueFactory(
            new PropertyValueFactory<Task, String>("returnDate"));      
        tcSelect.setCellValueFactory(
            new PropertyValueFactory<Task, String>("select")); 
        tvTasks.setItems(tvTasksData);
        tfTitle.setDisable(true);
        cbAssignment.setDisable(true);
        tcSelect.setEditable(false);
        imgAdd.setDisable(true);
    }
    
    private void close(){
        Stage stage = (Stage) tfTitle.getScene().getWindow();
        stage.close();
    }
        
}
