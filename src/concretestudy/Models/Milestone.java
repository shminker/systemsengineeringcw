/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy.Models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;

/**
 *
 * @author admin
 */
public class Milestone implements Serializable {
    String name;
    String assignment;
    ArrayList<Task> tasks = new ArrayList<>();
    //LocalDateTime setDate;
    //LocalDateTime dueDate;
    
    public Milestone(
            String name,
            String assignment,
            ArrayList<Task> tasks
            ){
        this.name = name;
        this.assignment = assignment;
        this.tasks = tasks;
    }
    
    public void addTask(Task t){
        tasks.add(t);
    }
    
    public boolean isComplete(){
        int n = tasks.size();
        int c = 0;
        for (Task t : tasks){
            if (t.isComplete()){
                c++;
            }
        }
        if(c==n){
            return true;
        }
        else {
            return false;
        }
    }
    
    public String getName(){
        return name;
    }
    public String getAssignmnet(){
        return assignment;
    }
    
//    public LocalDateTime getSetDate(){
//        return setDate;
//    }
//    
//    public LocalDateTime getReturnDate(){
//        return dueDate;
//    }
    public ArrayList<Task> getTasks(){
        return tasks;
    }
    @Override
    public String toString(){
        return name;
    }
}
