/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy.Models;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;

/**
 *
 * @author admin
 */
public class Assignment implements Serializable {
    
    String name;
    String moduleName; //@ak
    ArrayList<Task> tasks = new ArrayList<>(); 
    private LocalDateTime setDate,
                    returnDate;
    private int weight;
    boolean complete;
    String URI;
    boolean extended;
    private ArrayList<Milestone> milestones;    


    //Getters and Setters begin here
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    
    public String getModuleName() {
        return moduleName;
    }

    public void setModuleName(String name) {
        this.moduleName = name;
    }
    
    public LocalDateTime getSetDate() {
        return setDate;
    }

    public void setSetDate(LocalDateTime setDate) {
        this.setDate = setDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }

    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }

    public int getWeight() {
        return weight;
    }

    public void setWeight(int weight) {
        this.weight = weight;
    }
    //Getters and Setters end here
    
    
    @Override
    public String toString(){
//        return String.format("%s====\nSet:\t%s\nDue:\t%s\nWeight:\t%s\n",
//                name,
//                setDate.toString(),
//                returnDate.toString(),
//                weight
//        );
        return name;
    }
    
    
    public void addExtension(LocalDateTime newDeadline){
        returnDate = newDeadline;
    }
    
    public void addExtension(int days){
        //returnDate.add(Date, days);
    }
    
    public boolean isComplete(){
        return complete;
    }
    
    public void setComplete(boolean completetion){
        this.complete = completetion;
    }
    
    public Assignment(
            String name,
            LocalDateTime setDate,
            LocalDateTime returnDate,
            int weight
    ) {
        this.name = name;
        this.setDate = setDate;
        this.returnDate = returnDate;
        this.weight = weight;
        this.complete = false;
        this.milestones = new ArrayList<Milestone>();
    }
    
      
    public void setURI(String URI){
        this.URI = URI;
    }
    
    public String getURI(){
        return this.URI;
    }
    
    public Assignment(){
        this.name = "Default Assignment";
        this.weight = 0;
    }
    
    
    public Assignment(String name, int weight){
        this.name = name;
        this.weight = weight;
    } 
    
    public ArrayList<Task> getTasks() {
    return tasks;
    }

    public void addTasks(Task task) {
        tasks.add(task);
    }    
    
    public double getProgress(){
        double n = tasks.size();
        double c = 0;
        for(Task t : tasks){
            if (t.isComplete()){
                //System.out.println(t.getName() + "is complete");
                c++;
            }
        }
        return c/n;
    }
    
    public boolean isExtended(){
        return extended;
    }
    
    public void setExtention(boolean extention){
        extended = extention;
        if (extended){
           URI = "[E] " + URI;
        }
    }
    
   
    public void addMilestone(Milestone m) {
        this.milestones.add(m);
    }
    public ArrayList<Milestone> getMilestones() {
        return this.milestones;
    }
    
    public String getGCStart(){
        return String.format("%02d-%02d-%d",
            setDate.getDayOfMonth(),
            setDate.getMonthValue(),
            setDate.getYear());
    }
    public String getFriendlySetDate(){
        return String.format("%02d/%02d/%d %02d:%02d",
            setDate.getDayOfMonth(),
            setDate.getMonthValue(),
            setDate.getYear(),
            setDate.getHour(),
            setDate.getMinute()%60
        );
    }
    public String getFriendlyReturnDate(){
        return String.format("%02d/%02d/%d %02d:%02d",
            returnDate.getDayOfMonth(),
            returnDate.getMonthValue(),
            returnDate.getYear(),
            returnDate.getHour(),
            returnDate.getMinute()%60
        );
    }   
    public long getGCDuration(){
        return ChronoUnit.DAYS.between(setDate, returnDate);
    }
}