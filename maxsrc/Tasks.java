package studyplanner;

import java.util.ArrayList;

public class Tasks {
    
    private String name;
    private String notes;
    
    enum workType {
        studying, programming, writing
    }
   
    
    //milestone: (e.g. time for studying, chapters for reading)
    
    private ArrayList<Tasks> taskArray = new ArrayList<>();
    private ArrayList<Activities> activitiesArray = new ArrayList<>();



    //void: chkDependencies

    //void: addNote
    public String getNote(){
        return notes;
    }
    
    public void setNote(String inpNote){
        notes = inpNote;
    }

    //boolean: chkActivitesComplete
    public boolean chkActivitesComplete(){
        boolean allComplete = true;
        for(int i = 0; i < activitiesArray.size(); i++){
            if(activitiesArray.get(i).getComplete() == false){
                allComplete = false;
            }
        }
        return allComplete;
    }
    
    //void: addDependncies
    public void addTaskArray(Tasks task){
        taskArray.add(task);
    }

    //Function: getArrayOfActivities
    public ArrayList<Tasks> getArrayOfActivities(){
        return taskArray;
    }
    
    //Function: getTaskAtIndex
    public Tasks getTaskAtIndex(int index){
        return taskArray.get(index);
    }

    public ArrayList<Activities> getActivitiesArray(){
        return activitiesArray;
    }
    
}
