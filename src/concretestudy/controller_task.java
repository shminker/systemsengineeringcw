/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import concretestudy.Models.Module;
import concretestudy.Models.Activity;
import concretestudy.Models.Semester;
import concretestudy.Models.Assignment;
import concretestudy.Models.Task;
import java.io.IOException;
import java.net.URL;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.ComboBox;
import javafx.scene.control.DateCell;
import javafx.scene.control.DatePicker;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.control.TreeItem;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.Pane;
import javafx.stage.Modality;
import javafx.stage.Stage;


/**
 *
 * @author ashmit.khadka
 */
public class controller_task implements Initializable {    
    
    private Task task;
    
    @FXML
    private Label labelHeaderName;
    @FXML
    private Label labelHeaderInfo;    
    @FXML
    private TextField tf_title;    
    @FXML
    private ComboBox cb_assginement;    
    @FXML
    private ComboBox cb_dependency;    
    @FXML
    private DatePicker dp_start;
    @FXML
    private DatePicker dp_end;    
    @FXML
    private ComboBox cbStart;
    @FXML
    private ComboBox cbEnd;    
    @FXML
    private TextArea taNote;    
    @FXML
    private TextArea ta_note;    
    @FXML
    private Assignment assignment; 
    @FXML
    private ImageView imgAddTask;
    @FXML
    private ImageView imgDeleteTask;
    @FXML
    private ImageView imgEditTask;    
    @FXML
    private ImageView imgAddActivity; 
    @FXML 
    private TableView tvActivities;    
    @FXML
    private Pane taskForm; 
    @FXML
    private ProgressIndicator piProgress;    
    private controller_main parent_main;    
    private LocalDateTime now;
    private ObservableList<String> times =
                FXCollections.observableArrayList();
    private DateTimeFormatter dtf;
    enum viewMode {newTask, editTask, newActivities}
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {     
        dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm");
        //System.out.println("new task opened");    
        for (int h = 6; h<24; h++){
            for (int m = 0; m<60; m+=15){
                if (h<12){
                    times.add(String.format(
                    "%02d:%02d", h , m));
                }else {
                    times.add(String.format(
                    "%02d:%02d", h , m));
                }
            }
        }
        now = LocalDateTime.now();
    }
    
    public void set_main_controller(controller_main main){
        this.parent_main = main;
    }  
    
    public void setData(Semester s){
        //System.out.println(calToday.getTime());        
        for (Module m: parent_main.current_semester.getModules()){            
            for (Assignment a: m.getAssginments()){                  
                if(!a.isComplete() && now.isBefore(a.getReturnDate())){
                    //System.out.println(a.getReturnDate() + " > " + today.compareTo(a.getReturnDate()));
                    cb_assginement.getItems().add(a.getURI());
                    System.out.println(a.getName());
                }                    
            }
        }
    }
    //min date is today
    //missed assignment & complete are ignored
    public void chkAssignment(){
        if(!cb_assginement.getSelectionModel().isEmpty()){
            for(Module m : parent_main.current_semester.getModules()){
                for (Assignment a : m.getAssginments()){
                    if (a.getURI().equals(cb_assginement.getValue()) &&
                            !a.isComplete()){
                        assignment = a;
                        
                        LocalDate minDate;
                        if (now.isAfter(a.getSetDate())){
                            minDate = now.toLocalDate();
                        }
                        else{
                            minDate = assignment.getSetDate().toLocalDate();
                        }
                        LocalDate maxDate = assignment.getReturnDate().toLocalDate();                        
                                                   
                        dp_start.setDayCellFactory((p) -> new DateCell() {
                        @Override
                            public void updateItem(LocalDate ld, boolean bln) {
                                super.updateItem(ld, bln);
                                setDisable(ld.isBefore(minDate) || ld.isAfter(maxDate));
                            }
                        });
                        
                        ObservableList<String> startTimes = FXCollections.observableArrayList(times);             
                        startTimes.remove("23:45");
                        for(String stime: startTimes){
                            LocalTime time = LocalDateTime.parse("2000-01-01 " + stime, dtf).toLocalTime();
                            if(time.isAfter(now.toLocalTime())){
                                cbStart.getItems().add(stime);
                            }
                        }

                        
                        dp_start.setDisable(false);
                        cbStart.setDisable(false);
                    }            
                }
            }
        }
    }
    
    public void chkDateStart(){
        dp_end.setValue(null);
        cbEnd.setValue("");
        if(dp_start.getValue() != null &&
                !cbStart.getSelectionModel().isEmpty()){
            
            LocalDate minDate = dp_start.getValue();
            LocalDate maxDate = assignment.getReturnDate().toLocalDate();
            
            dp_end.setDayCellFactory((p) -> new DateCell() {
            @Override
                public void updateItem(LocalDate ld, boolean bln) {
                    super.updateItem(ld, bln);
                    setDisable(ld.isBefore(minDate) || ld.isAfter(maxDate));
                }
            });
            ObservableList<String> startTimes = FXCollections.observableArrayList(times);             
            for (String t: times){
                System.out.println(t);
                startTimes.remove(t);
                if (t.equals(cbStart.getValue())){
                    break;
                }
            }
            cbEnd.getItems().addAll(startTimes);       

            dp_end.setDisable(false);
            cbEnd.setDisable(false);
        }
        else{            
            dp_end.setDisable(true);
            cbEnd.setDisable(true);

        }
    }
    
    public void addTask() throws Exception{
        
        if(tf_title.getText().isEmpty()){
            System.out.println("text empty");
        }
        else if(cb_assginement.getSelectionModel().isEmpty()){
            System.out.println("cb a empty");
        }
        else if(dp_start.getValue() == null){
            System.out.println("no starting date");
        }
        else if(cbStart.getSelectionModel().isEmpty()){
            System.out.println("cb start empty");
        }
        else if(dp_end.getValue() == null){
            System.out.println("cb a empty");
        }
        else if(cbEnd.getSelectionModel().isEmpty()){
            System.out.println("cb a empty");
        }     
        else{
            Assignment a = parent_main.current_semester.getAssginement(
                    cb_assginement.getValue().toString());
            if (a == null){
                close();
            }
            String name = tf_title.getText();
            LocalDateTime dateStart = LocalDateTime.parse(String.format(
                    "%s %s", dp_start.getValue(), cbStart.getValue()), dtf);
            LocalDateTime dateEnd = LocalDateTime.parse(String.format(
                    "%s %s", dp_end.getValue(), cbEnd.getValue()), dtf);
            String note;
            if(taNote.getText().isEmpty()){
                note = "";
            } else{
                note = taNote.getText();
            }
            Duration duration = Duration.between(dateStart, dateEnd);
            System.out.println(String.format(
                    "%s %s", dp_start.getValue(), cbStart.getValue()));            
            task = new Task(name, dateStart, dateEnd, note, duration); 
            task.setURI(cb_assginement.getValue().toString());
            a.addTasks(task);  
            stageMode(2);
        }
    }
    public void selectActivity(){
        tvActivities.setOnMouseClicked(event -> {
        if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
            System.out.println("clicked!");
            Object selected_item = null;
            selected_item = tvActivities.getSelectionModel().getSelectedItem();
            if (selected_item instanceof Activity){
                Activity a = (Activity)selected_item;
                System.out.println(a.getName());                
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_activity.fxml"));
                    Parent root1 = (Parent) fxmlLoader.load();
                    fxmlLoader.<controller_activity>getController().showActivity(a);
                    controller_activity ctr_activity = fxmlLoader.getController();
                    Stage stage = new Stage();
                    stage.setTitle("Activity");
                    stage.setScene(new Scene(root1));  
                    stage.setOnHidden(e -> ctr_activity.close());
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.showAndWait();
                    updateActivities();
                }
                catch (Exception e){

                }
            }  
        }
        }); 
    }
    
    public void addActivity(){        
        try {
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_activity.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            fxmlLoader.<controller_activity>getController().setParentTask(task);            
            Stage stage = new Stage();
            stage.setTitle("New Activity");
            stage.setScene(new Scene(root1));
            stage.initModality(Modality.APPLICATION_MODAL);
            stage.showAndWait();            
        } catch (IOException ex) {
            Logger.getLogger(controller_task.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        updateActivities();
    }
    
    public void deleteTask(){
        System.out.println("hello?");
        for (Module m : parent_main.current_semester.getModules()){
            System.out.println(m.getName());
        }
    }
    
    
    public void stageMode(int mode){
        if (mode == 1){
            this.tvActivities.setDisable(true);
        } else if (mode == 2){           
            labelHeaderName.setText(task.getName());
            labelHeaderInfo.setText(String.format("Remaining: %s, Activities: %s",                    
                    String.format("%02d:%02d:%02d",
                            task.getTimeRemaining().toHours(),
                            task.getTimeRemaining().toMinutes()%60,
                            task.getTimeRemaining().getSeconds()%60),                                                        
                    task.getActivitiesArray().size()));
            System.out.println(task.getProgress());
            piProgress.setProgress(task.getProgress());
            if (task.getAvailableTime().isZero()){
                imgAddActivity.setDisable(true);
            }            
            this.tvActivities.setDisable(false);
            this.taskForm.setDisable(true);            
        } else if (mode == 3){            
            tvActivities.setDisable(false);            
            taskForm.setDisable(true);
            
        }        
    }
    public void updateActivities(){
        stageMode(2);        
        tvActivities.getColumns().clear();
        ObservableList<Activity> tvData = 
                FXCollections.observableArrayList();    
        for (Activity a: task.getActivitiesArray()){
            tvData.add(a);
        }
        TableColumn tvNameCol = new TableColumn("Activity");
        tvNameCol.setMinWidth(100);
        tvNameCol.setCellValueFactory(
                new PropertyValueFactory<Activity, String>("name")); 
        TableColumn tvTypeCol = new TableColumn("Type");
        tvTypeCol.setMinWidth(100);
        tvTypeCol.setCellValueFactory(
                new PropertyValueFactory<Activity, String>("type")); 
        TableColumn tvTimeCol = new TableColumn("Time");        
        tvTimeCol.setMinWidth(100);
        tvTimeCol.setCellValueFactory(
                new PropertyValueFactory<Activity, String>("FriendlyRemainingTime")); 

        
        tvActivities.getColumns().addAll(
                tvNameCol,
                tvTypeCol,
                tvTimeCol);
        tvActivities.setItems(tvData);

    }
    
    public void showTask(Task parentTask){
        this.task = parentTask;        
        this.tf_title.setText(task.getName());        
        this.taNote.setText(task.getNote());
        this.cb_assginement.setValue(task.getURI());
        this.dp_start.setValue(task.getSetDate().toLocalDate());
        this.dp_end.setValue(task.getReturnDate().toLocalDate());
        this.cbStart.setValue(task.getSetDate().toLocalTime());
        this.cbEnd.setValue(task.getReturnDate().toLocalTime());
        
        task.isComplete();
        updateActivities();        
    }
  
    
    
    private void close(){
        Stage stage = (Stage) tf_title.getScene().getWindow();
        stage.close();
    }
}