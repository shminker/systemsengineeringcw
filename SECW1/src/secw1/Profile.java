package secw1;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class Profile {

    private String name;
    private int id;
    private int year;
    private ArrayList<Semester> semesters;

    public Profile() {

    }

    public Profile(String name, int year, ArrayList<Semester> semesters) {
        this.name = name;
        this.year = year;
        this.semesters = semesters;
    }

    public String getName() {
        return name;
    }

    public int getId() {
        return id;
    }

    public int getYear() {
        return year;
    }

    public ArrayList<Semester> getSemesters() {
        return semesters;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public void setSemesters(ArrayList<Semester> semesters) {
        this.semesters = semesters;
    }

    public void addSemester(Semester s) {
        this.semesters.add(s);
    }

    public void rmvSemester(Semester s) {
        this.semesters.remove(s);
    }

    public void readData(String fileName) {
        String dataline;
        int numModules;
        ArrayList<Module> modules = new ArrayList<>();
        try {
            BufferedReader in = new BufferedReader(new FileReader(fileName));
            int studentID = Integer.parseInt(in.readLine());
            if (studentID == id) {
                while ((dataline = in.readLine()) != null) {
                    semesters.add(readSemLine(dataline));
                    numModules = semesters.get(semesters.size()-1).getNumModules();
                    
                    for (int i = 0; i < numModules; i++) {
                        modules.add(readModLine(in.readLine()));
                    }
                    semesters.get(semesters.size()-1).setModules(modules);
                }
            } else {
                System.out.println("Data was not read in as the user doesn't have permission to access this file");
            }
        } catch (IOException e) {
            System.out.println("Data file couldn't be read in");
        }

    }

    public Semester readSemLine(String semLine) {
        String name;
        String begin;
        String end;
        int numModules;
        Scanner s = new Scanner(semLine);
        s.useDelimiter(",");
        name = s.next();
        begin = s.next();
        end = s.next();
        numModules = Integer.parseInt(s.next());
        
        Semester sem = new Semester(name, begin, end, numModules);

        return sem;
    }

    public Module readModLine(String modLine) {
        String modName;
        int numCw;
        int numExams;
        

        Scanner s = new Scanner(modLine);
        s.useDelimiter(",");
        
        
        
        modName = s.next();
        numCw = Integer.parseInt(s.next());
        numExams = Integer.parseInt(s.next());

        ArrayList<Assignment> assignments = new ArrayList<>();
        
        String[] cwLine = new String[numCw];
        String[] examLine = new String[numExams];
        
        for (int i = 0; i < numCw; i++) {
            cwLine[i] = s.next();
            Scanner cwS = new Scanner(cwLine[i]);
            cwS.useDelimiter(" ");
            assignments.add(new Assignment(cwS.next(),cwS.next(),cwS.next()));
            cwS.useDelimiter("\n");
            assignments.get(assignments.size()-1).setName(cwS.next());
        }

        for (int i = 0; i < numExams; i++) {
            examLine[i] = s.next();
            Scanner examS = new Scanner(examLine[i]);
            examS.useDelimiter(" ");
            assignments.add(new Assignment(examS.next(),examS.next(),examS.next()));
            examS.useDelimiter("\n");
            assignments.get(assignments.size()-1).setName(examS.next());
        }
        
       Module m = new Module(modName,assignments); 
       
       return m;

    }

}
