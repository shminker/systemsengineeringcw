/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.Initializable;
import concretestudy.Models.*;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.fxml.FXML;
import javafx.scene.control.Label;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
;

/**
 *
 * @author ashmit.khadka
 */
public class controller_profile implements Initializable {
    
    private controller_main parentCM;
    @FXML
    private Label lbName;
    @FXML
    private Label lbCourse;
    @FXML
    private Label lbID;
    @FXML
    private Label lbYear;
  
    @Override
    public void initialize(URL url, ResourceBundle rb) {        
        
    }   
    public void setData(controller_main cm){
        parentCM = cm;
        showProfile();
        
    }
    public void readSemesterFile(){
        Stage stage = new Stage();
        stage.setTitle("File Chooser Sample");
        final FileChooser fileChooser = new FileChooser();   
        try {
            FileReader fr = new FileReader(fileChooser.showOpenDialog(stage));
            parentCM.current_semester = Semester.loadSemester(fr);
        } catch (FileNotFoundException ex) {
            Logger.getLogger(controller_profile.class.getName()).log(Level.SEVERE, null, ex);
        }
        parentCM.showDashboard();
        showProfile();
        
    }
    public void showProfile(){
        lbName.setText(parentCM.current_semester.getName());
        lbCourse.setText(parentCM.current_semester.getCourse());
        lbID.setText(String.format("%s - %s",
                parentCM.current_semester.getFaculty(),
                parentCM.current_semester.getSID()));
        lbYear.setText(String.format("Year %s, Semester %s",
                parentCM.current_semester.getYear(),
                parentCM.current_semester.getSemester()));        
    }
    
            
   
}