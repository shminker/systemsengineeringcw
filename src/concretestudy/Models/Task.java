/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy.Models;

import java.io.Serializable;
import java.sql.Time;
import java.time.Duration;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Date;
import javafx.scene.control.CheckBox;

/**
 *
 * @author admin
 */
public class Task implements Serializable {
    
    private String name;
    private LocalDateTime setDate,
                    returnDate;
    private boolean complete;
    private String URI;    
    private Duration duration;
    private String note;
    transient private CheckBox select = new CheckBox();
    
    enum workType {
        studying, programming, writing
    }
   
    
    
    private ArrayList<Task> taskArray = new ArrayList<>();
    private ArrayList<Activity> activitiesArray = new ArrayList<>();


    
    public Task(String name, LocalDateTime setDate, LocalDateTime returnDate, String note, Duration duration){
        this.name = name;
        this.setDate = setDate;
        this.returnDate = returnDate;
        this.note = note;
        this.duration = duration;
        this.complete = false;
    }

    //void: addNote
    public String getNote(){
        return note;
    }
    
    public String getName(){
        return name;
    }
    
    public void setNote(String inpNote){
        note = inpNote;
    }

    //boolean: chkActivitesComplete
    public boolean chkActivitesComplete(){
        boolean allComplete = true;
        for(int i = 0; i < activitiesArray.size(); i++){
            
        }
        return allComplete;
    }
    
    //void: addDependncies
    public void addTaskArray(Task task){
        taskArray.add(task);
    }
    
    public void addActivity(Activity activity){
        activitiesArray.add(activity);
    }
    
    //Function: getArrayOfActivities
    public ArrayList<Task> getArrayOfActivities(){
        return taskArray;
    }
    
    //Function: getTaskAtIndex
    public Task getTaskAtIndex(int index){
        return taskArray.get(index);
    }

    public ArrayList<Activity> getActivitiesArray(){
        return activitiesArray;
    }
    
    public boolean isComplete(){
        
        int n = activitiesArray.size();
        int c = 0;
        for(Activity a : activitiesArray){
            if (a.isComplete()){
                c++;
            }
        }
        if (n == 0){
            return false;
        }
        else if (n == c){
            complete = true;
        } else {
            complete = false;
        }
        
        return complete;
    }
    
    public void setComplete(boolean completetion){
        this.complete = completetion;
    }
    
    public LocalDateTime getSetDate() {
        return setDate;
    }

    public void setSetDate(LocalDateTime setDate) {
        this.setDate = setDate;
    }

    public LocalDateTime getReturnDate() {
        return returnDate;
    }
    
    public void setReturnDate(LocalDateTime returnDate) {
        this.returnDate = returnDate;
    }
    
    public int getActivityCount(){
        return activitiesArray.size();
    }
    
    public void setURI(String URI){
        this.URI = URI;
    }
    
    public String getURI(){
        return this.URI;
    }
    
    public void setTime(Duration time){
        this.duration = time;
    }
    
    public Duration getTime(){
        return this.duration;
    }  
    
    
    
    //Time for tasks
    public Duration getAvailableTime(){
        Duration usedTime = Duration.ofMillis(0);
        for (Activity a : activitiesArray){
            usedTime = usedTime.plus(a.getTime());
        }
        return this.duration.minus(usedTime);
    }
    
    
    public Duration getTimeRemaining(){
        Duration durationCompleted = Duration.ofMillis(0);
        for (Activity a : activitiesArray){
            durationCompleted = durationCompleted.plus(a.getTime().minus(a.getTimeRemaining()));
        }
        return this.duration.minus(durationCompleted);
    }
    
    public double getProgress(){
        long total = duration.toMillis();
        long completed = getTimeRemaining().toMillis(); 
        double progress = ((double)(total-completed)/total);
        if (complete){
            return 1;
        }else {
            return ((double)(total-completed)/total);
        }
    }
    
    public CheckBox getSelect(){
        return select;
    } 
    
    public void setSelect(CheckBox select){
        this.select = select;
    }
    
    public String getGCStart(){
        return String.format("%02d-%02d-%d",
            setDate.getDayOfMonth(),
            setDate.getMonthValue(),
            setDate.getYear());
    }
    public long getGCDuration(){
        return ChronoUnit.DAYS.between(setDate, returnDate);
    }
    
    public String getFriendlySetDate(){
        return String.format("%02d/%02d/%d %02d:%02d",
            setDate.getDayOfMonth(),
            setDate.getMonthValue(),
            setDate.getYear(),
            setDate.getHour(),
            setDate.getMinute()%60
        );
    }
    public String getFriendlyReturnDate(){
        return String.format("%02d/%02d/%d %02d:%02d",
            returnDate.getDayOfMonth(),
            returnDate.getMonthValue(),
            returnDate.getYear(),
            returnDate.getHour(),
            returnDate.getMinute()%60
        );
    }   
     
    @Override
    public String toString(){
        return name;
    }
    
    
    
}