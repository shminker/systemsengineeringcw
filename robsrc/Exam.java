package seng;

import java.util.Calendar;


public class Exam extends Assignment{
    
    public Exam(String name, Calendar setDate, Calendar returnDate, int weight){
        super(name, setDate, returnDate, weight);
    }
    
    public Exam(){
        super();
    }
}