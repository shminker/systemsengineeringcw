/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import java.net.URL;
import java.sql.Time;
import java.time.Duration;
import java.util.ResourceBundle;
import java.util.Timer;
import java.util.TimerTask;
import java.util.TreeMap;
import java.util.concurrent.TimeUnit;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import concretestudy.Models.Activity;
import concretestudy.Models.Task;


/**
 *
 * @author ashmit.khadka
 */
public class controller_activity implements Initializable {
    Activity activity;
    Task parentTask;        
    @FXML
    private Label lbHeader;    
    @FXML
    private Label lbInfo;    
    @FXML
    private AnchorPane apTimer;    
    @FXML
    private AnchorPane apForm;  
    @FXML
    private Label lbTime;
    @FXML
    private ImageView imgAction;
    private final Image playIcon = new Image(getClass().getResourceAsStream(
                   "Resources/img/icons/play-button.png"));
    private final Image pauseIcon = new Image(getClass().getResourceAsStream(
                   "Resources/img/icons/pause.png"));

    @FXML
    private TextField tfTitle;    
    @FXML
    private ComboBox cbType;    
    @FXML
    private ComboBox cbDuration;    
    @FXML
    private TextArea taNote;    
    TreeMap<String, Duration> alltimes = new TreeMap<>();     
    enum studyType{Reading, Writing, Coding, Revising}    
    boolean study;
    Timer timer;
    TimerTask timerTask;
    Time ctime;
    long activityDuration; 
    Duration activityRemaining;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {   
        study = false; 
        timer = new Timer();

    }   
    public void setParentTask(Task task){
        this.parentTask = task;
        setData();
    }    
    
    public void setData(){
        Duration duration = parentTask.getAvailableTime();
        long totalMinutes = duration.toMinutes();        
        for(int m = 15; m<=totalMinutes; m+=15){             
                alltimes.put(
                        String.format("%02d hour(s) : %02d minute(s)", m/60, m%60),
                        Duration.ofMinutes(m));                
        }   
        System.out.println(alltimes);
        cbType.setItems(FXCollections.observableArrayList(studyType.values()));
        cbDuration.getItems().addAll(alltimes.keySet());
    }
    
    public void showActivity(Activity a){
        this.activity = a;    
        lbHeader.setText(a.getName());
        lbInfo.setText("Ready");
        timer = new Timer();
        System.out.println(activity.getTime());
        activityRemaining = activity.getTimeRemaining();
        lbTime.setText(String.format("%02d:%02d:%02d",
                activityRemaining.toHours(),
                activityRemaining.toMinutes()%60,
                activityRemaining.getSeconds()%60));
        apForm.setDisable(true);
        apTimer.setDisable(false);
        this.tfTitle.setText(activity.getName());
        this.cbType.setValue(activity.getType());
        this.cbDuration.setValue(String.format("%02d:%02d:%02d",
                activity.getTime().toHours(),
                activity.getTime().toMinutes()%60,
                activity.getTime().getSeconds()%60));
        this.taNote.setText(activity.getNote());
    }
    
    public void addActivity(){              
        if (tfTitle.getText().isEmpty() ||
                cbType.getValue() == null ||
                cbDuration.getValue() == null
                ){  
        } else {
            String note = "";
            if (!taNote.getText().isEmpty()){
                note = taNote.getText();
            }
            Activity activity = new Activity(
                    tfTitle.getText(),
                    cbType.getValue().toString(),
                    alltimes.get(cbDuration.getValue()),
                    note
            );            
            parentTask.addActivity(activity);

        }
        close();        
    }

    public void activityStart(){
        timer.cancel();                    
        timer = new Timer();
        timer.scheduleAtFixedRate(new TimerTask() {
            public void run() {
                if(activityRemaining.toMillis() > 0)
                {                    
                    activityRemaining = activityRemaining.minusMillis(1000);
                    activity.setTimeRemaining(activityRemaining);
                    Platform.runLater(() -> {
                        lbTime.setText(String.format("%02d:%02d:%02d",
                            TimeUnit.MILLISECONDS.toHours(activityRemaining.toMillis()),
                            TimeUnit.MILLISECONDS.toMinutes(activityRemaining.toMillis())%60,
                            TimeUnit.MILLISECONDS.toSeconds(activityRemaining.toMillis())%60));
                    });
                }
                else{
                    timer.cancel();                    
                }    
            }
        }, 1000,1000);      
    }
    public void timerAction(){
        if (study){
            study = false;
            this.imgAction.setImage(playIcon);
            timer.cancel();            
            lbInfo.setText(activity.getType() + " Paused");
        }else{
            study = true;
            this.imgAction.setImage(pauseIcon);
            activityStart();
            lbInfo.setText(activity.getType());
        }
    }
    
    public void close(){
        System.out.println("closing..");
        timer.cancel();
        Stage stage = (Stage) tfTitle.getScene().getWindow();
        stage.close();
    }   
}