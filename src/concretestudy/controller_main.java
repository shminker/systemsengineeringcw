/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package concretestudy;

import concretestudy.Models.Module;
import concretestudy.Models.Activity;
import concretestudy.Models.Semester;
import concretestudy.Models.Milestone;
import concretestudy.Models.Task;
import concretestudy.Models.Assignment;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.URL;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.time.Duration;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.ProgressBarTableCell;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.GaussianBlur;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.MouseButton;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import javafx.stage.Modality;
import javafx.stage.Stage;



/**
 *
 * @author ashmit.khadka
 */
public class controller_main implements Initializable {
    
    Semester current_semester;
    
    @FXML
    private WebView webView;
    @FXML
    private VBox main_container;    
    @FXML
    private ImageView main_item_img;  
    
    private Image imgDashboard = new Image(getClass().getResourceAsStream(
            "Resources/img/icons/home.png"));
    private Image imgModule = new Image(getClass().getResourceAsStream(
            "Resources/img/icons/agenda.png"));
    private Image imgAssignment = new Image(getClass().getResourceAsStream(
            "Resources/img/icons/alarm-clock.png"));
    private Image imgTask = new Image(getClass().getResourceAsStream(
            "Resources/img/icons/list.png"));
    @FXML
    private TableView tv_a;
    @FXML
    private TableView tv_b;
    @FXML
    private TableView tv_c;
    @FXML
    private Label labelHeaderName;
    @FXML
    private Label labelHeaderInfo;
    @FXML
    private Label main_day;
    @FXML
    private Label main_month;
    @FXML
    private Label main_day_month;    
    @FXML
    private Label tableheader_1; 
    @FXML
    private Label tableheader_2; 
    @FXML
    private Label tableheader_3; 
    private LocalDateTime now;
    
    private GaussianBlur gaussianBlur = new GaussianBlur();  
    
    enum viewMode{Dashboard, Assignment, Module};
    private viewMode view;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) { 
        //updateCalender();        
        initialize_obj();
        for (Module m : current_semester.getModules()){
            for (Assignment a: m.getAssginments()){
                a.setURI(String.format(
                        "%s ▶ %s", m.getName(), a.getName()));
                for (Task t : a.getTasks()){
                    t.setURI(String.format(
                            "%s ▶ %s", a.getURI(), t.getName()));
                }
            }
        }
        try{
            ObjectInputStream ois = new ObjectInputStream(new FileInputStream("semester.bin"));
            current_semester = (Semester) ois.readObject();
        }catch (Exception e){
            System.out.println("error while loading semester profile");
        }
        now = LocalDateTime.now();
        showDashboard();
        URL gc = this.getClass().getResource("/concretestudy/gc/main.html");
        WebEngine engine = webView.getEngine();
        engine.load(gc.toString());       
    }   
    
    public void updateCalender(){
        Calendar today = Calendar.getInstance();
        Date date = today.getTime();
        main_day.setText(new SimpleDateFormat("EEEE", Locale.ENGLISH).format(date.getTime()));
        main_month.setText(new SimpleDateFormat("MMMM", Locale.ENGLISH).format(date.getTime()));
        int day = today.get(Calendar.DAY_OF_MONTH);
        switch (day){
            case 1 : main_day_month.setText(day + "st"); break;
            case 2 : main_day_month.setText(day + "nd"); break; 
            case 3 : main_day_month.setText(day + "rd"); break;
            default: main_day_month.setText(day + "th"); break;
        }
        
    }
    
    public void showViewProfile() {
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_profile.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();    
            fxmlLoader.<controller_profile>getController().setData(this);            
            Stage stage = new Stage();
            stage.setTitle("Profoile");
            stage.setScene(new Scene(root1));  
            stage.initModality(Modality.APPLICATION_MODAL);
            blur(true);
            stage.showAndWait();
            updateView();
            showDashboard();

            blur(false);
        }
        catch (Exception e){
            
        }
    }
    
    public void showViewMilestone(){
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_milestone.fxml"));
            Parent root1 = (Parent) fxmlLoader.load(); 
            fxmlLoader.<controller_milestone>getController().setData(current_semester);  
            Stage stage = new Stage();
            stage.setTitle("Milestone");
            stage.setScene(new Scene(root1));  
            stage.initModality(Modality.APPLICATION_MODAL);
            blur(true);
            stage.showAndWait();
            updateView();
            showDashboard();

            blur(false);            
        }
        catch (Exception e){
            
        }
    }

    public void showViewTask(){
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_task.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            fxmlLoader.<controller_task>getController().set_main_controller(this);              
            controller_task ctr_task = fxmlLoader.getController();
            ctr_task.setData(current_semester); 
            ctr_task.stageMode(1);
            Stage stage = new Stage();
            stage.setTitle("Task");
            stage.setScene(new Scene(root1));  
            stage.initModality(Modality.APPLICATION_MODAL);
            blur(true);
            stage.showAndWait();
            updateView();
            showDashboard();

            blur(false);
        }
        catch (Exception e){
            
        }
    }
    
     public void showViewNav(){
        try{
            FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_navigator.fxml"));
            Parent root1 = (Parent) fxmlLoader.load();
            fxmlLoader.<controller_navigator>getController().set_main_controller(this);              
            controller_navigator ctr_deadlines = fxmlLoader.getController();
            ctr_deadlines.setData(current_semester);           
            Stage stage = new Stage();
            stage.setTitle("Navigator");
            stage.setScene(new Scene(root1));  
            stage.initModality(Modality.APPLICATION_MODAL);
            blur(true);
            stage.showAndWait();
            updateView();
            //showDashboard();
            blur(false);
        }
        catch (Exception e){   
            
        }
    }    
    
    /**
     * Shows the Dashboard and all Assignments.
     */ 
    public void showDashboard(){
        updateView();
        //Initialize view elements values    
        main_item_img.setImage(imgDashboard); 
        labelHeaderName.setText("Dashboard");
        int currentHour = now.getHour();
        if (currentHour >= 6 && currentHour < 12){
            labelHeaderInfo.setText("Good Morining, " + current_semester.getName());
        }else if (currentHour >= 12 && currentHour < 18){
            labelHeaderInfo.setText("Good Afternoon, " + current_semester.getName());
        }else{
            labelHeaderInfo.setText("Good Evening, " + current_semester.getName());
        }

        //Clear previous data:
        tv_a.getColumns().clear();
        tv_b.getColumns().clear();
        tv_c.getColumns().clear(); 
        
        //build table
        ObservableList<Assignment> tv_a_data = 
                FXCollections.observableArrayList();        
        ObservableList<Assignment> tv_b_data = 
                FXCollections.observableArrayList();        
        ObservableList<Assignment> tv_c_data = 
                FXCollections.observableArrayList(); 
        
        for(Module m: current_semester.getModules()){
            for (Assignment a: m.getAssginments()){
                a.setModuleName(m.getName());
                System.out.println(a.getName() + " complete " + a.isComplete());
                if (a.isComplete()){
                    tv_a_data.add(a); 
                }
                else if (now.isBefore(a.getReturnDate())) {
                    tv_b_data.add(a);
                }
                else{
                    tv_c_data.add(a);                    
                }
            }
        }
        tableheader_1.setText(String.format(
                "Completed (%d)", tv_a_data.size()));
        tableheader_2.setText(String.format(
                "Upcoming (%d)", tv_b_data.size()));
        tableheader_3.setText(String.format(
                "Missed (%d)", tv_c_data.size()));
        //COMPLETE TABLE
        TableColumn tv_complete_subjectCol = new TableColumn("Module");
        tv_complete_subjectCol.setMinWidth(100);
        tv_complete_subjectCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("moduleName")); 
        
        TableColumn tv_complete_firstNameCol = new TableColumn("Name");
        tv_complete_firstNameCol.setMinWidth(100);
        tv_complete_firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("name")); 
        
        TableColumn tv_complete_weightCol = new TableColumn("Weight");
        tv_complete_weightCol.setMinWidth(50);
        tv_complete_weightCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("weight")); 
        
        TableColumn tv_complete_statusCol = new TableColumn("Status");
        tv_complete_statusCol.setMinWidth(20);
        tv_complete_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_complete_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());

        
        tv_a.getColumns().addAll(
                tv_complete_subjectCol,
                tv_complete_firstNameCol, 
                tv_complete_weightCol,
                tv_complete_statusCol);   
        tv_a.setItems(tv_a_data);
        
        //UPCOMMING TABLE
        TableColumn tv_b_subjectCol = new TableColumn("Module");
        tv_b_subjectCol.setMinWidth(100);
        tv_b_subjectCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("moduleName")); 
        
        TableColumn tv_b_firstNameCol = new TableColumn("Name");
        tv_b_firstNameCol.setMinWidth(100);
        tv_b_firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("name")); 
        
        TableColumn tv_b_weightCol = new TableColumn("Weight");
        tv_b_weightCol.setMinWidth(50);
        tv_b_weightCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("weight")); 
        
        TableColumn tv_b_statusCol = new TableColumn("Status");
        tv_b_statusCol.setMinWidth(20);
        tv_b_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_b_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        tv_b.getColumns().addAll(
                tv_b_subjectCol,
                tv_b_firstNameCol,
                tv_b_weightCol,
                tv_b_statusCol);   
        tv_b.setItems(tv_b_data);
        
        //MISSED TABLE
        TableColumn tv_c_subjectCol = new TableColumn("Module");
        tv_c_subjectCol.setMinWidth(100);
        tv_c_subjectCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("moduleName")); 
        
        TableColumn tv_c_firstNameCol = new TableColumn("Name");
        tv_c_firstNameCol.setMinWidth(100);
        tv_c_firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("name")); 
        
        TableColumn tv_c_weightCol = new TableColumn("Weight");
        tv_c_weightCol.setMinWidth(50);
        tv_c_weightCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("weight")); 
        
        TableColumn tv_c_statusCol = new TableColumn("Status");
        tv_c_statusCol.setMinWidth(20);
        tv_c_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_c_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        tv_c.getColumns().addAll(
                tv_c_subjectCol,
                tv_c_firstNameCol,
                tv_c_weightCol,
                tv_c_statusCol);   
        tv_c.setItems(tv_c_data);    

    }
    
    /**
     * Shows all Assignments for a selected Module.
     */ 
    public void showModule(Module m){
        updateView();        
        //Initialize view elements values    
        main_item_img.setImage(imgModule); 
        labelHeaderName.setText(m.getName());
        this.labelHeaderInfo.setText(String.format(
                "Total Assginements - %d", m.getAssginments().size()));

        //Clear previous data:
        tv_a.getColumns().clear();
        tv_b.getColumns().clear();
        tv_c.getColumns().clear(); 
        
        //build table
        ObservableList<Assignment> tv_a_data = 
                FXCollections.observableArrayList();        
        ObservableList<Assignment> tv_b_data = 
                FXCollections.observableArrayList();        
        ObservableList<Assignment> tv_c_data = 
                FXCollections.observableArrayList(); 
        
        for (Assignment a: m.getAssginments()){
            a.setModuleName(m.getName());
            if (a.isComplete()){
                tv_a_data.add(a); 
            }
            else if (now.compareTo(a.getReturnDate())<=0) {
                tv_b_data.add(a);
            }
            else{
                tv_c_data.add(a);
            }
        }        
        tableheader_1.setText(String.format(
                "Completed (%d)", tv_a_data.size()));
        tableheader_2.setText(String.format(
                "Upcoming (%d)", tv_b_data.size()));
        tableheader_3.setText(String.format(
                "Missed (%d)", tv_c_data.size()));
        //COMPLETE TABLE
        TableColumn tv_complete_subjectCol = new TableColumn("Module");
        tv_complete_subjectCol.setMinWidth(100);
        tv_complete_subjectCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("moduleName")); 
        
        TableColumn tv_complete_firstNameCol = new TableColumn("Name");
        tv_complete_firstNameCol.setMinWidth(100);
        tv_complete_firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("name")); 
        
        TableColumn tv_complete_weightCol = new TableColumn("Weight");
        tv_complete_weightCol.setMinWidth(50);
        tv_complete_weightCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("weight")); 
        
        TableColumn tv_complete_statusCol = new TableColumn("Status");
        tv_complete_statusCol.setMinWidth(20);
        tv_complete_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_complete_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());        
        tv_a.getColumns().addAll(
                tv_complete_subjectCol,
                tv_complete_firstNameCol, 
                tv_complete_weightCol,
                tv_complete_statusCol);   
        tv_a.setItems(tv_a_data);
        
        //UPCOMMING TABLE
        TableColumn tv_b_subjectCol = new TableColumn("Module");
        tv_b_subjectCol.setMinWidth(100);
        tv_b_subjectCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("moduleName")); 
        
        TableColumn tv_b_firstNameCol = new TableColumn("Name");
        tv_b_firstNameCol.setMinWidth(100);
        tv_b_firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("name")); 
        
        TableColumn tv_b_weightCol = new TableColumn("Weight");
        tv_b_weightCol.setMinWidth(50);
        tv_b_weightCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("weight")); 
        
        TableColumn tv_b_statusCol = new TableColumn("Status");
        tv_b_statusCol.setMinWidth(20);
        tv_b_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_b_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        tv_b.getColumns().addAll(
                tv_b_subjectCol,
                tv_b_firstNameCol,
                tv_b_weightCol,
                tv_b_statusCol);   
        tv_b.setItems(tv_b_data);
        
        //MISSED TABLE
        TableColumn tv_c_subjectCol = new TableColumn("Module");
        tv_c_subjectCol.setMinWidth(100);
        tv_c_subjectCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("moduleName")); 
        
        TableColumn tv_c_firstNameCol = new TableColumn("Name");
        tv_c_firstNameCol.setMinWidth(100);
        tv_c_firstNameCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("name")); 
        
        TableColumn tv_c_weightCol = new TableColumn("Weight");
        tv_c_weightCol.setMinWidth(50);
        tv_c_weightCol.setCellValueFactory(
                new PropertyValueFactory<Assignment, String>("weight")); 
        
        TableColumn tv_c_statusCol = new TableColumn("Status");
        tv_c_statusCol.setMinWidth(20);
        tv_c_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_c_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        tv_c.getColumns().addAll(
                tv_c_subjectCol,
                tv_c_firstNameCol,
                tv_c_weightCol,
                tv_c_statusCol);   
        tv_c.setItems(tv_c_data);    

    } 

    
    /**
    * Shows all Tasks for a selected Assignment.
    */ 
    public void showAssignment(Assignment a){
        updateView();
        SimpleDateFormat format = new SimpleDateFormat("EE dd MMMM hh:mm aa");
        
        //Initialize view elements values    
        main_item_img.setImage(imgModule); 
        labelHeaderName.setText(String.format(
                "%s ▶ %s", a.getModuleName(),a.getName()));
        this.labelHeaderInfo.setText(String.format("Set: %s, Due: %s, %s%%", 
                a.getFriendlySetDate(),
                a.getFriendlyReturnDate(),
                a.getWeight()));

        //Clear previous data:
        tv_a.getColumns().clear();
        tv_b.getColumns().clear();
        tv_c.getColumns().clear(); 
        
        //build table
        ObservableList<Task> tv_a_data = 
                FXCollections.observableArrayList();        
        ObservableList<Object> tv_b_data = 
                FXCollections.observableArrayList();        
        ObservableList<Task> tv_c_data = 
                FXCollections.observableArrayList(); 
        
        //today.setYear(Calendar.getInstance().get(Calendar.YEAR));        
        for (Task t: a.getTasks()){
                System.out.println(a.getName() + " complete " + a.isComplete());
            if (t.isComplete()){
                tv_a_data.add(t); 
            }
            else if (now.compareTo(t.getReturnDate())<=0) {
                tv_b_data.add(t);
            }
            else{
                tv_c_data.add(t);
            }
        }
        for (Milestone m : a.getMilestones()){
            tv_b_data.add(m);
        }
        tableheader_1.setText(String.format(
                "Completed (%d)", tv_a_data.size()));
        tableheader_2.setText(String.format(
                "Upcoming (%d)", tv_b_data.size()));
        tableheader_3.setText(String.format(
                "Missed (%d)", tv_c_data.size()));
        
        //COMPLETE TABLE
        TableColumn tv_a_nameCol = new TableColumn("Name");
        tv_a_nameCol.setMinWidth(100);
        tv_a_nameCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("name")); 
        
        TableColumn tv_a_dueCol = new TableColumn("Due");
        tv_a_dueCol.setMinWidth(100);
        tv_a_dueCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("FriendlyReturnDate")); 
        
        TableColumn tv_a_actsCol = new TableColumn("Activites");
        tv_a_actsCol.setMinWidth(50);
        tv_a_actsCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("activityCount")); 
        
        TableColumn tv_complete_statusCol = new TableColumn("Status");
        tv_complete_statusCol.setMinWidth(20);
        tv_complete_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_complete_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        tv_a.getColumns().addAll(
                tv_a_nameCol,
                tv_a_dueCol, 
                tv_a_actsCol,
                tv_complete_statusCol);   
        tv_a.setItems(tv_a_data);        
        
        
        //UPCOMING TABLE
        TableColumn tv_b_nameCol = new TableColumn("Name");
        tv_b_nameCol.setMinWidth(100);
        tv_b_nameCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("name")); 
        
        TableColumn tv_b_setCol = new TableColumn("Set");
        tv_b_setCol.setMinWidth(100);
        tv_b_setCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("FriendlySetDate")); 
        
        TableColumn tv_b_dueCol = new TableColumn("Due");
        tv_b_dueCol.setMinWidth(100);
        tv_b_dueCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("FriendlyReturnDate")); 
        
        TableColumn tv_b_actsCol = new TableColumn("Activites");
        tv_b_actsCol.setMinWidth(50);
        tv_b_actsCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("activityCount")); 
        
        TableColumn tv_b_statusCol = new TableColumn("Status");
        tv_b_statusCol.setMinWidth(20);
        tv_b_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_b_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        tv_b.getColumns().addAll(
                tv_b_nameCol,
                tv_b_setCol,
                tv_b_dueCol,
                tv_b_actsCol,
                tv_b_statusCol);   
        tv_b.setItems(tv_b_data);
        
        //MISSED TABLE
        TableColumn tv_c_nameCol = new TableColumn("Name");
        tv_c_nameCol.setMinWidth(100);
        tv_c_nameCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("name")); 
        
        TableColumn tv_c_dueCol = new TableColumn("Due");
        tv_c_dueCol.setMinWidth(100);
        tv_c_dueCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("FriendlySetDate")); 
        
        TableColumn tv_c_actsCol = new TableColumn("Activites");
        tv_c_actsCol.setMinWidth(50);
        tv_c_actsCol.setCellValueFactory(
                new PropertyValueFactory<Task, String>("activityCount")); 
        
        TableColumn tv_c_statusCol = new TableColumn("Status");
        tv_c_statusCol.setMinWidth(20);
        tv_c_statusCol.setCellValueFactory(new PropertyValueFactory<Assignment, Double>(
        "progress"));
        tv_c_statusCol.setCellFactory(ProgressBarTableCell.<Assignment> forTableColumn());
        
        
        tv_c.getColumns().addAll(
                tv_c_nameCol,
                tv_c_dueCol,
                tv_c_actsCol,
                tv_c_statusCol);   
        tv_c.setItems(tv_c_data); 
    } 
    
    public void selectedTVA(){
        selectTableObject(tv_a);
    }
    
    public void selectedTVB(){
        selectTableObject(tv_b);
    }
    
    public void selectedTVC(){
        selectTableObject(tv_c);
    }
    
    public void selectTableObject(TableView parentTV){
        parentTV.setOnMouseClicked(event -> {
        if(event.getButton().equals(MouseButton.PRIMARY) && event.getClickCount() == 2) {
            System.out.println("clicked!");
            Object selected_item = null;
            selected_item = parentTV.getSelectionModel().getSelectedItem();
            if (selected_item instanceof Assignment){
                Assignment a = (Assignment)selected_item;
                showAssignment(a);
            }else if (selected_item instanceof Task){
                Task t = (Task)selected_item; 
                try{
                    FXMLLoader fxmlLoader = new FXMLLoader(getClass().getResource("view_task.fxml"));
                    Parent root1 = (Parent) fxmlLoader.load();
                    fxmlLoader.<controller_task>getController().showTask(t);
                    Stage stage = new Stage();
                    stage.setTitle("Task task");
                    stage.setScene(new Scene(root1));  
                    stage.initModality(Modality.APPLICATION_MODAL);
                    stage.showAndWait();
                }
                catch (Exception e){

                }
                
            }  
        }
        }); 
        
    }
    
    public void blur(boolean set){
        if (set){
            gaussianBlur.setRadius(10); 
        }else {
            gaussianBlur.setRadius(0); 
        }
        main_container.setEffect(gaussianBlur);
    }    
    
    
    public void updateGanttChart() throws Exception{
        ArrayList<String> links = new ArrayList<>();
        int cnt = 0;    
        int linkCnt = 0;
        String p = Paths.get(".").toAbsolutePath().normalize().toString() + 
                "\\build\\classes\\concretestudy\\gc\\testdata.js";

        PrintWriter w = new PrintWriter(p);
        w.printf("%s\n\t%s\n",
                "var demo_tasks = {", "\"data\":[");
        for (Module m : current_semester.getModules()){ 
            cnt++;   
            int mCnt = cnt;
            w.printf("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"progress\": 1, \"open\": true},\n",
                    mCnt, m.getName(),m.getStartDate() , m.length());
            for (Assignment a : m.getAssginments()){
                cnt++;
                int aCnt = cnt;
                w.printf("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n",
                        cnt, a.getName(), a.getGCStart(), a.getGCDuration(), mCnt, a.getProgress());
                links.add(String.format("\t{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"%d\"},",
                        ++linkCnt, mCnt, aCnt, 1));
                for (Task t : a.getTasks()){
                    cnt++;
                    w.printf("\t\t{\"id\":%d, \"text\":\"%s\", \"start_date\":\"%s\", \"duration\":\"%d\", \"parent\":\"%d\", \"progress\":%f, \"open\": true},\n",
                            cnt, t.getName(), t.getGCStart(), t.getGCDuration(), aCnt, t.getProgress());
                    links.add(String.format("\t{\"id\":\"%d\",\"source\":\"%d\",\"target\":\"%d\",\"type\":\"%d\"},",
                        ++linkCnt, aCnt, cnt, 1));
                }
            }                  
        }
        w.printf("],\n\"links\":[\n");
        for(String link : links){
            w.println(link);
        }
        w.printf("\t]\n};");
        w.close();
        webView.getEngine().reload();
    }
    
    public void updateView(){
        try {
            updateGanttChart();
            updateCalender();
            ObjectOutputStream oos = new ObjectOutputStream(new FileOutputStream("semester.bin"));
            oos.writeObject(current_semester);   
        } catch (Exception ex) {
            System.out.println(ex.fillInStackTrace());
        }
        
             
    }
    
    public void initialize_obj(){
        Semester s1 = new Semester("Natalia Grealish", "CMP", "Computing Science", 2, 200400800, 2);        
        Module m1 = new Module("Programming 2", "", LocalDateTime.of(2019, 1, 1, 12 ,0), LocalDateTime.of(2019, 5, 10, 15, 0));
        
        Assignment a1 = new Assignment("Corsework 1",           //complete
                LocalDateTime.of(2019, 1, 15, 12, 0), LocalDateTime.of(2019, 2, 6, 15, 0), 30);
        a1.setComplete(true);
        Assignment a2 = new Assignment("Corsework 2",           //upcoming
                LocalDateTime.of(2019, 2, 28, 12, 0), LocalDateTime.of(2019, 5, 1, 15, 0), 30);  
        Assignment a3 = new Assignment("Exam 1",                //upcoming
                LocalDateTime.of(2019, 5, 31, 14, 15), LocalDateTime.of(2019, 5, 31, 16, 15), 40);
        
        Task t1 = new Task("Exercise 1", LocalDateTime.of(2019, 1, 15, 12, 0),  LocalDateTime.of(2019, 1, 15, 15, 0), "testNote", Duration.ofMinutes(90));
        t1.setComplete(true);
        Task t2 = new Task("Exercise 2", LocalDateTime.of(2019, 1, 20, 9, 0), LocalDateTime.of(2019, 1, 20, 13, 0), "testNote", Duration.ofMinutes(120)); 
        t2.setComplete(true);            


        Activity t1a1 = new Activity("Create the classes", "Programming", Duration.ofMinutes(60), "testNote");
        t1a1.setTimeRemaining(Duration.ofMinutes(0));
        t1a1.setComplete(true);
        Activity t1a2 = new Activity("Test the code", "Programming", Duration.ofMinutes(30), "testNote");
        t1a2.setTimeRemaining(Duration.ofMinutes(0));
        t1a2.setComplete(true);

        Activity t2a1 = new Activity("Create UML class daigram", "Writting", Duration.ofMinutes(90), "testNote"); 
        t2a1.setTimeRemaining(Duration.ofMinutes(0));
        t2a1.setComplete(true);

        Activity t2a2 = new Activity("Define requirements", "Writting", Duration.ofMinutes(60), "testNote"); 
        t2a2.setTimeRemaining(Duration.ofMinutes(0));
        t2a2.setComplete(true);

        
        t1.addActivity(t1a1);
        t1.addActivity(t1a2);
        t2.addActivity(t2a1);
        t2.addActivity(t2a2);        
        a1.addTasks(t1);
        a1.addTasks(t2);        
        m1.addAssignment(a1); 
        m1.addAssignment(a2);
        m1.addAssignment(a3);
        s1.addModules(m1);
        
        Assignment a21 = new Assignment("Corsework 1",      //missed
                LocalDateTime.of(2019, 2, 15, 12, 0), LocalDateTime.of(2019, 5, 1, 15, 0), 60);
        Assignment a22 = new Assignment("Exam",             //upcoming
                LocalDateTime.of(2019, 1, 1, 12, 0), LocalDateTime.of(2019, 4, 10, 15, 0), 40);        
        Module m2 = new Module("Graphics 1", "", LocalDateTime.of(2019, 1, 1, 12 ,0), LocalDateTime.of(2019, 5, 10, 15, 0));
        m2.addAssignment(a21); 
        m2.addAssignment(a22);       
        s1.addModules(m2);

        Module m3 = new Module("Data Stuctures & Algorithums", "", LocalDateTime.of(2019, 1, 1, 12 ,0), LocalDateTime.of(2019, 5, 10, 15, 0));

        Assignment a31 = new Assignment("Corsework 1",      //complete
                LocalDateTime.of(2019, 2, 7, 12, 0), LocalDateTime.of(2019, 3, 10, 15, 0), 15); 
        a31.setComplete(false);
        Assignment a32 = new Assignment("Corsework 2",      //upcoming
                LocalDateTime.of(2019, 1, 1, 12, 0), LocalDateTime.of(2019, 4, 21, 15, 0), 35);
        Assignment a33 = new Assignment("Exam 1",           //upcoming
                LocalDateTime.of(2019, 1, 1, 12, 0), LocalDateTime.of(2019, 4, 10, 15, 0), 50);  
        
        Task t1a31 = new Task("Exercise 1", LocalDateTime.of(2019, 2, 8, 10, 0),  LocalDateTime.of(2019, 2, 8, 15, 0), "testNote", Duration.ofMinutes(300));        
        Task t2a31 = new Task("Planning", LocalDateTime.of(2019, 2, 12, 9, 0),  LocalDateTime.of(2019, 2, 12, 12, 0), "testNote", Duration.ofMinutes(180));
        
        Activity t1a31act1 = new Activity("Create nodes", "Writting", Duration.ofMinutes(200), "testNote"); 
        t1a31act1.setTimeRemaining(Duration.ofMinutes(130));
        Activity t1a31act2 = new Activity("Create methods", "Writting", Duration.ofMinutes(100), "testNote");
        t1a31act2.setTimeRemaining(Duration.ofMinutes(40));
        
        Activity t2a31act1 = new Activity("UML daigram", "Writting", Duration.ofMinutes(100), "testNote"); 
        t2a31act1.setTimeRemaining(Duration.ofMinutes(0));
        t2a31act1.setComplete(true);
        Activity t2a31act2 = new Activity("Write psudo code", "Writting", Duration.ofMinutes(80), "testNote");
        t2a31act2.setTimeRemaining(Duration.ofMinutes(0));
        t2a31act2.setComplete(true);

        
        t1a31.addActivity(t1a31act1);
        t1a31.addActivity(t1a31act2);        
        t2a31.addActivity(t2a31act1);
        t2a31.addActivity(t2a31act2);        
        a31.addTasks(t1a31);
        a31.addTasks(t2a31);
        m3.addAssignment(a31); 
        m3.addAssignment(a32);
        m3.addAssignment(a33);
        s1.addModules(m3);
        
        Assignment a41 = new Assignment("Stage 1",      //missed
                LocalDateTime.of(2019, 1, 25, 12, 0), LocalDateTime.of(2019, 3, 20, 15, 0), 40);
        Assignment a42 = new Assignment("Stage 2",             //upcoming
                LocalDateTime.of(2019, 2, 12, 12, 0), LocalDateTime.of(2019, 5, 2, 15, 0), 60);        
        Module m4 = new Module("Software Engineering", "", LocalDateTime.of(2019, 1, 1, 12 ,0), LocalDateTime.of(2019, 5, 10, 15, 0));
        m4.addAssignment(a41); 
        m4.addAssignment(a42);       
        s1.addModules(m4);       
        
        

        current_semester = s1;        
        
        
    }   

    
}
